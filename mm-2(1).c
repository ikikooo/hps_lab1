#include <stdio.h>
#include <omp.h>
#define MAX 100
int is1[MAX],is2[MAX];//原数组is1，临时空间数组is2
void merge(int low,int mid,int high){
  int i=low,j=mid+1,k=low;
  //#pragma omp master
  while(i<=mid&&j<=high)
    if(is1[i]<is1[j])
      is2[k++]=is1[i++];
    else
      is2[k++]=is1[j++];
  //#pragma omp master
  while(i<=mid)
    is2[k++]=is1[i++];
//#pragma omp master
  while (j<=high)
    is2[k++]=is1[j++];
  for ( i = low; i <=high ; i++) {
    is1[i]=is2[i];
  }
 
}
 
void mergeSort(int a, int b){
  if(a<b){
    int mid=(a+b)/2;
	#pragma omp parallel sections
	{
		#pragma omp section
    mergeSort(a,mid);
#pragma omp section
    mergeSort(mid+1,b);
	}
    merge(a,mid,b);
  }
}
 
void main(){
  int i,n,count;
  printf("inpute thread：");
  scanf("%d",&count);
  printf("inpute nums：");
  scanf("%d",&n);
  printf("every array：\n");
  for ( i = 1; i <=n ; ++i) {
    scanf("%d",&is1[i]);
  }
  omp_set_num_threads(count);
  double t1=omp_get_wtime();
  mergeSort(1,n);
  double t2=omp_get_wtime();
  printf("after sort：\n");
  for ( i = 1; i <=n ; ++i) {
    printf("%4d",is1[i]);
  }
  printf("\n\nellipse times: %f s\n",t2-t1);
}
