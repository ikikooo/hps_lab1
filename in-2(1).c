#include <stdio.h> 
#include <stdlib.h> 
#include <omp.h>
 #define MAX 100
 int a[MAX];
//插入排序 
void InsertSort(int *a,int len); 
 
//输出数组中的元素 
void OutputArray(int *a, int len); 
 
void main() 
{ 
   int i,n,count;
  printf("inpute thread：");
  scanf("%d",&count);
  printf("inpute nums：");
  scanf("%d",&n);
  printf("every array：\n"); 
 for ( i = 0; i <n ; ++i) {
    scanf("%d",&a[i]);
  }
  omp_set_num_threads(count);
   double t1=omp_get_wtime();
  InsertSort(a,n); 
 double t2=omp_get_wtime();
  printf("after sort：\n"); 
  OutputArray(a,n); 
 printf("\n\nellipse times: %f s\n",t2-t1);
  
} 
 
//插入排序 
void InsertSort(int *a,int len) 
{ 
#pragma omp parallel for
  for(int i=1;i<len;i++) 
  { 
    int j=i-1; 
     
    int temp=a[i];//需要插入的数据 
//#pragma omp parallel sections
//{
//#pragma omp section
    while(temp<a[j] && j>=0)//当插入的数据小于前面的数据时 
    { 
      a[j+1]=a[j];//将插入的数据的前面的数据向后移动 
       
      j--; 
    } 
//#pragma omp section 
    a[++j]=temp;//插入数据 
  }

  //}  
} 
 

void OutputArray(int *a, int len) 
{ 
  for(int i=0; i<len; i++) 
  { 
    printf("%d ",a[i]); 
  } 
 
  printf("\n"); 
} 

